#!/bin/bash
permission-changing () {
    # Make the script executable and copy it to the correct place.
    chmod +x ./Script_for_syncing/syncing.sh
    cp ./Script_for_syncing/syncing.sh /etc/archlinux-cache/syncfile.sh
}

copying-systemd () {
    # copying systemd files to /etc/systemd/system.
    cp ./ServiceFiles/syncfile.service /etc/systemd/system/
    cp ./ServiceFiles/syncfile.timer /etc/systemd/system/
}

enabling-systemd-timer () {
    # enable and start the timer. Also starts the service.
    systemctl enable --now syncfile.timer
    systemctl start syncfile.service
}


if [ $UID = 0 ]
    then
        permission-changing
        copying-systemd
        enabling-systemd-timer
    else 
        echo "Need to run this script with sudo or as root!"
        exit 0
    fi